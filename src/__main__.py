from random import random
from src import Particle, Universe, Canvas
import numpy as np
from argparse import ArgumentParser

parser = ArgumentParser()


bg_color = (0, 16, 40)
resolution = (720, 720)
fps = 60


try:
    with Canvas(resolution, fps, px_per_unit=1, preview=False, render=True) as canvas:
        u = Universe(canvas, bg_color)
        for _ in range(400):
            u.add_particle(Particle(
                pos=(2 * np.random.rand(2) - 1) * u.canvas.size * 0.8,
                vel=(2 * np.random.rand(2) - 1) * u.canvas.size * 0.05,
                mass=20.0 * random(),
                color=255 - np.array(bg_color),
                shape="(",
                canvas=u.canvas))
        u.loop(max_ticks=fps * 60)
except KeyboardInterrupt:
    pass
